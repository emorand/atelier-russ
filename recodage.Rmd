---
title: "Le bon , la brute et le recodage"
description: |
  Quelques méthodes pour recoder une variable
output:
  distill::distill_article
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Verifier l'importation

R est aussi un logiciel de statistique.  Il convient donc de verifier que les données disponibles dans R sont bien les mêmes que celle du tableur  (nombre de lignes , nombre de colonnes, type de variables). Après importation, par défaut, le nom du tableau dans R est le nom du fichier importé.

On prend notre script à deux mains et on écrit deux petites lignes de commande 

```{r,eval =FALSE, echo=TRUE}
str(essai)
summary(essai)
```

(le raccourci clavier est ctrl entree)

Syntaxe R , on applique des fonctions à des objets.

La première donne des informations sur le tableau de données 

```{r resultats_str ,echo=TRUE}

str(essai)
```

La seconde fait quelques descriptions (ne l'utiliser pas sans deux petites minutes de reflexion avant exécution)

```{r resultats summary}
summary(essai)
```

La variable region est quantitative discret (int. dans R) , or on veut la traiter comme une variable qualitative. Il y a deux problèmes :
- Il faut changer le type
- Les modalités pourraient avoir des intitulés plus claires que "1" et "2".

# Type de données

## 1.2.3, ..importer

Au moment de l'importation vous pouvez choisir "from text (base)'  ou 'from text (readr)' .
La seconde option ouvre une fenêtre interactive dans laquelle on peut choisir le type de variable

## La coercition
```{r}
str(essai)
```
On peut changer manuellement le type d'une colonne , ici à l'ancienne
 et attention à la casse !!
 
```{r, eval=FALSE}
essai$region<-as.character(essai$region)
```

 
```{r}
essai$Region<-as.character(essai$Region)
```
Le type a changé mais c'est toujours pas joli


## et tout ce qui a été raconté dans la matinée











